# Ansible Role: OS Image Deployment

This Role will deploy a standard baseline O/S configuration image for Redhat/OracleLinux 8 and above hardening the image using [openscap](https://www.open-scap.org/getting-started/) (STIG) applying the medium/hard mitigations. This will be deployed and utilised using [Packer](https://www.packer.io/plugins/provisioners/ansible/ansible) at a later date. A broader overview of the components used are:

- https://docs.ansible.com/ansible-core/devel/playbook_guide/playbooks_templating.html
- https://docs.ansible.com/ansible/latest/user_guide/vault.html
- https://docs.ansible.com/ansible-core/devel/playbook_guide/playbooks_variables.html
- https://github.com/hashicorp/packer-plugin-ansible
- https://www.packer.io/plugins/provisioners/ansible/ansible-local

***

## Dependencies
Prior to initial deployment, follow the [connection methods](https://docs.ansible.com/ansible/latest/user_guide/connection_details.html) to establish a user defined in the Role Variables section  _{{ myuser }}_ prior to running on the target host with the required privileges.

OracleLinux 8 release media does not ship with _libselinux-python_ installed so you will initially encounter the following issue when attempting to run tasks against host.
```bash
"msg": "Aborting, target uses selinux but python bindings (libselinux-python) aren't installed!"
```
To mitigate this issue and others its also configured with the following none [UEK](https://blogs.oracle.com/scoter/post/oracle-linux-and-unbreakable-enterprise-kernel-uek-releases) 4.18.x kernel.

**NOTE** RedHat release [media](https://developers.redhat.com/products/rhel/download) does not encounter this issue. 

The successful deployment of this role requires the target host image deployment to have a FQDN matching the logic for all the templated logic to work. Ensure you have ran `hostnamectl set-hostname OS-build.FQDN` ahead of any deployments for e.g. `dom[1-4].com`.

Once these steps have been completed, a full deployment can be carried out as per installation. Integration with Packer will be completed at a later date to fully utilise the deployment with [Builders](https://developer.hashicorp.com/packer/docs/builders)

As CarbonBlack is part of the deployment, these will need to be deployed beforehand or available for this component to work as well as the CBR Servers. IPtable rules have been omitted from the rulesets  that are provided, so these would need to be added additonally as well, all of which are out of scope of this role.

## Requirements
If you do not require certain components, disable the required roles in `requirements.yml` and run as per installation steps, however the following will be deployed as standard.
- `logrotate`
- `changelog`
- `carbonblack`

## Role Variables
```
# CHRONY
chrony_conf_1_dom1: 10.x.x.x 
chrony_conf_2_dom1: 10.x.x.x 
chrony_conf_1_dom2: 10.x.x.x
chrony_conf_2_dom2: 10.x.x.x
chrony_conf_1_dom3: 10.x.x.x
chrony_conf_2_dom3: 10.x.x.x
chrony_conf_1_dom4: 10.x.x.x
chrony_conf_2_dom4: 10.x.x.x

# DNS
dns_dom1_1: "10.x.x.x"
dns_dom1_2: "10.x.x.x"
dns_dom2_1: "10.x.x.x"
dns_dom2_2: "10.x.x.x"
dns_dom3_1: "10.x.x.x"
dns_dom3_2: "10.x.x.x"
dns_dom4_1: "10.x.x.x"
dns_dom4_2: "10.x.x.x"

# CHANGELOG
action_completed: "Deployed: CarbonBlack,"
cbvers: "v7.1.1.92158" # CHANGE this when upgrading
cbr_repo: "http://10.x.x.x/foobar/carbonblack/{{ cbvers }}"

# CarbonBlack Archives
dl_site1_dom1: "CarbonBlackLinuxInstaller-{{ cbvers }}-SITE1ServerLinux.tar.gz" 
dl_site2_dom1: "CarbonBlackLinuxInstaller-{{ cbvers }}-SITE2ServerLinux.tar.gz" 
dl_site3_dom1: "CarbonBlackLinuxInstaller-{{ cbvers }}-SITE3ServerLinux.tar.gz" 
dl_dom2: "CarbonBlackLinuxInstaller-{{ cbvers }}-DOM2.tar.gz" 
dl_dom3: "CarbonBlackLinuxInstaller-{{ cbvers }}-DOM3.tar.gz" 

# CBR Servers
cbr_site1_dom1: "10.x.x.x"   
cbr_site2_dom1: "10.x.x.x"  
cbr_dom2:  "10.x.x.x"     
cbr_dom3: "10.x.x.x"   
cbr_dom4: "10.x.x.x"   

# CHANGELOG
adm_grp: Admins
myuser: user1
update_action: O/S baseline deployment
action_completed: "Deployed: CarbonBlack,"
usr_id: ME
```

## Installation
```bash
$ ansible-galaxy install -r requirements.yml

$ ansible-playbook  --extra-vars '@passwd.yml' deploy.yml -bK
```

```yaml
---
- hosts:
  - all
  become: yes
  gather_facts: yes
  roles:
    - roles/ansible-packer-build
    - roles/logrotate
    - roles/changelog

  post_tasks:
   - name: Setup/Install logrotate
     ansible.builtin.include: roles/logrotate/tasks/main.yml

   - name: Install changelog baseline
     ansible.builtin.include: roles/changelog/initial_deploy.yml

   - name: Clean up .bash_history
     ansible.builtin.include: roles/ansible-packer-build/tasks/cleanup.yml

   - name: Reboot host
     ansible.builtin.include: roles/ansible-packer-build/tasks/reboot.yml

   - name: Openscap hardening mitigations (high)
     ansible.builtin.include: roles/ansible-packer-build/tasks/high.yml

   - name: Openscap hardening mitigations (medium)
     ansible.builtin.include: roles/ansible-packer-build/tasks/medium.yml

   - name: Deploy CarbonBlack
     ansible.builtin.include: roles/carbonblack/tasks/main.yml
```

## Licence

MIT/BSD
